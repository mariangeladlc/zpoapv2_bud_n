## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Tue Jun 01 2021 08:28:36 GMT+0000 (Coordinated Universal Time)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.2.0|
|**Generation Platform**<br>SAP Business Application Studio|
|**Floorplan Used**<br>2masterdetail|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://10.130.8.81:8001/sap/opu/odata/sap/ZPOAPV_BUD_PROJECT_SRV
|**Module Name**<br>zpoapv2_bud_n|
|**Application Title**<br>zpoapv2_bud_n|
|**Namespace**<br>com.stulz|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>1.77.1|
|**Enable Telemetry**<br>True|

## zpoapv2_bud_n

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


