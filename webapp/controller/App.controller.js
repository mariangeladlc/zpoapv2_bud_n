sap.ui.define(
  ["./BaseController", "sap/ui/model/json/JSONModel","sap/m/MessageToast"],
  function (BaseController, JSONModel,MessageToast) {
    "use strict";

    return BaseController.extend("com.stulz.zpoapv2budn.controller.App", {
      onInit: function () {
        var oViewModel,
          fnSetAppNotBusy,
          iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();

        oViewModel = new JSONModel({
          busy: true,
          delay: 0,
          layout: "OneColumn",
          previousLayout: "",
          actionButtonsInfo: {
            midColumn: {
              fullScreen: false,
            },
          },
        });
        this.setModel(oViewModel, "appView");

        fnSetAppNotBusy = function () {
          oViewModel.setProperty("/busy", false);
          oViewModel.setProperty("/delay", iOriginalBusyDelay);
        };

        // since then() has no "reject"-path attach to the MetadataFailed-Event to disable the busy indicator in case of an error
        this.getOwnerComponent()
          .getModel()
          .metadataLoaded()
          .then(fnSetAppNotBusy);
        this.getOwnerComponent()
          .getModel()
          .attachMetadataFailed(fnSetAppNotBusy);

        // apply content density mode to root view
        this.getView().addStyleClass(
          this.getOwnerComponent().getContentDensityClass()
        );
      },
            openDialogApprove: function () {
        var text = "";
        if (this.getMode() == "None") {
          if (this.getDetail() != null) {
            text = text + "\u000a" + " - " + this.getDetail().Ebeln;
            this._getDialogApprove().open();
            sap.ui.getCore().byId("idFragment--approvedList").setText(text);
          } else {
            MessageToast.show("Nessun ordine di acquisto selezionato.");
          }
        } else {
          var oList = this.getList();
          var oBinding = oList.getBinding("items");
          var items = oList.getSelectedItems();
          if (items.length > 0) {
            for (var i = 0; i < items.length; i++) {
              var item = items[i];
              var context = item.getBindingContext();
              var obj = context.getProperty(null, context);
              text = text + "\u000a" + " - " + obj.Ebeln;
            }
            this._getDialogApprove().open();
            sap.ui.getCore().byId("idFragment--approvedList").setText(text);
          } else {
            MessageToast.show("Nessun ordine di acquisto selezionato.");
          }
        }
      },

      /*
      openDialogApprove: function () {
        var text = "";
        var oList = this.getList();
        var oBinding = oList.getBinding("items");
        var items = oList.getSelectedItems();
        if(items.length > 0){
            for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var context = item.getBindingContext();
            var obj = context.getProperty(null, context);
            text = text + "\u000a" + " - " + obj.Ebeln;
            }
            this._getDialogApprove().open();
            sap.ui.getCore().byId("idFragment--approvedList").setText(text);        
        }else{
            MessageToast.show("Nessun elemento selezionato. \u000a Spunta un elemento dalla lista");
        }
      },
     
      openDialogReject: function () {
        var text = "";
        var oList = this.getList();
        var oBinding = oList.getBinding("items");
        var items = oList.getSelectedItems();
        if(items.length > 0){
            for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var context = item.getBindingContext();
            var obj = context.getProperty(null, context);
            text = text + "\u000a" + " - " + obj.Ebeln;
            }
            this._getDialogReject().open();
            sap.ui.getCore().byId("idFragment--rejectList").setText(text);        
        }else{
            MessageToast.show("Nessun elemento selezionato. \u000a Spunta un elemento dalla lista");
        }
      },
       */
          openDialogReject: function () {
        var text = "";
        console.log(this.getMode());
        if (this.getMode() == "None") {
          if (this.getDetail() != null) {
            text = text + "\u000a" + " - " + this.getDetail().Ebeln;
            this._getDialogReject().open();
            sap.ui.getCore().byId("idFragment--rejectList").setText(text);
          } else {
            MessageToast.show("Nessun ordine di acquisto selezionato.");
          }
        } else {
          var oList = this.getList();
          var oBinding = oList.getBinding("items");
          var items = oList.getSelectedItems();
          if (items.length > 0) {
            for (var i = 0; i < items.length; i++) {
              var item = items[i];
              var context = item.getBindingContext();
              var obj = context.getProperty(null, context);
              text = text + "\u000a" + " - " + obj.Ebeln;
            }
            this._getDialogReject().open();
            sap.ui.getCore().byId("idFragment--rejectList").setText(text);
          } else {
            MessageToast.show("Nessun ordine di acquisto selezionato.");
          }
        }
      },

      closeDialogApprove: function () {
        this._oDialogApprove.close();
      },
      closeDialogReject: function () {
        this._oDialogReject.close();
      },

      _getDialogApprove: function () {
        if (!this._oDialogApprove) {
          this._oDialogApprove = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zpoapv2budn.view.DialogApprove",
            this
          );
          this.getView().addDependent(this._oDialogApprove);
        }
        return this._oDialogApprove;
      },
      _getDialogReject: function () {
        if (!this._oDialogReject) {
          this._oDialogReject = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zpoapv2budn.view.DialogReject",
            this
          );
          this.getView().addDependent(this._oDialogReject);
        }
        return this._oDialogReject;
      },
      /*
      approva: function () {
        var that = this;
        var oList = this.getList();
        var oBinding = oList.getBinding("items");
        var items = oList.getSelectedItems();
        var list = [];
        if(items.length > 0){
            for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var context = item.getBindingContext();
            var obj = context.getProperty(null, context);
            var object = {};
            object.Ebeln = obj.Ebeln;
            object.TestoApv = sap.ui
                .getCore()
                .byId("idFragment--richiesta_nota")
                .getValue();
            object.Frgco = obj.Frgco;
            object.FlagBudget = obj.FlagBudget;
            object.StatoApv = "1";
            list.push(object);
            }
            var header = {
            Id: "1",
            OdaSet: list,
            };
            this.approvaSingle(header);
        }else{
            MessageToast.show("Nessun elemento selezionato. \u000a Spunta un elemento dalla lista");
        }
      },
      */

      approva: function () {
        if (this.getMode() == "None") {
          this.approvaModeNone();
        } else {
          this.approvaModeMulti();
        }
      },
      approvaModeNone: function () {
        var that = this;
        var header = {
          Id: "1",
          OdaSet: [],
        };
        var list = [];
        var obj = this.getDetail();
        console.log(this.getDetail());
        if (obj != null) {
          var object = {};
          object.Ebeln = obj.Ebeln;
          object.Frgco = obj.Frgco;
          object.FlagBudget = obj.FlagBudget;
          object.TestoApv = sap.ui
            .getCore()
            .byId("idFragment--richiesta_nota")
            .getValue();
          object.StatoApv = "1";
          list.push(object);
          header.OdaSet = list;
          this.approvaSingle(header);
        } else {
            MessageToast.show("Nessun ordine di acquisto selezionato.");
        }
      },

      approvaModeMulti: function () {
        var that = this;
        var oList = this.getList();
        var oBinding = oList.getBinding("items");
        var items = oList.getSelectedItems();
        var header = {
          Id: "1",
          OdaSet: [],
        };
        var list = [];
        if (items.length > 0) {
          for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var context = item.getBindingContext();
            var obj = context.getProperty(null, context);
            var object = {};
            object.Ebeln = obj.Ebeln;
            object.Frgco = obj.Frgco;
            object.FlagBudget = obj.FlagBudget;
            object.TestoApv = sap.ui
              .getCore()
              .byId("idFragment--richiesta_nota")
              .getValue();
            object.StatoApv = "1";
            list.push(object);
          }
          header.OdaSet = list;
          this.approvaSingle(header);
        } else {
          MessageToast.show("Nessun ordine di acquisto selezionato.");
        }
      },
      approvaSingle: function (object) {
        var that = this;
        console.log(object);
        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.create("/ContainerSet", object, {
          success: function (oRetrievedResult) {
            MessageToast.show("Salvata");
            sap.ui.getCore().byId("idFragment--richiesta_nota").setValue("");

            that.closeDialogApprove();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
            that.onCloseDetailPress();
            this.setDetail(null);
            this.setList(null);

          },
          error: function (oError) {
            console.log(oError);
            that.closeDialogApprove();
          },
        });
      },
      /*
      rifiuta: function () {
        var that = this;
        var oList = this.getList();
        var oBinding = oList.getBinding("items");
        var items = oList.getSelectedItems();
        var list = [];
        if(items.length > 0){
            for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var context = item.getBindingContext();
            var obj = context.getProperty(null, context);
            var object = {};
            object.Ebeln = obj.Ebeln;
            object.TestoApv = sap.ui
                .getCore()
                .byId("idFragment--richiesta_nota")
                .getValue();
            object.StatoApv = "2";
            object.Frgco = obj.Frgco;
            object.FlagBudget = obj.FlagBudget;
            list.push(object);
            }
            var header = {
            Id: "1",
            OdaSet: list,
            };
            this.rifiutaSingle(header);
        }else{
            MessageToast.show("Nessun elemento selezionato. \u000a Spunta un elemento dalla lista");            
        }
      },
      */
           rifiuta: function () {
        console.log(this.getMode());
        if (this.getMode() == "None") {
          this.rifiutaModeNone();
        } else {
          this.rifiutaModeMulti();
        }
      },
      rifiutaModeNone() {
        console.log(this.getDetail());
        var that = this;
        var obj = this.getDetail();
        var list = [];
        if (obj != null) {
          var object = {};
          object.Ebeln = obj.Ebeln;
          object.Frgco = obj.Frgco;
          object.FlagBudget = obj.FlagBudget;
          object.TestoApv = sap.ui
            .getCore()
            .byId("idFragment--richiesta_nota_reject")
            .getValue();
          object.StatoApv = "2";
          list.push(object);

          var header = {
            Id: "1",
            OdaSet: list,
          };
          this.rifiutaSingle(header);
        } else {
          MessageToast.show("Nessun ordine di acquisto selezionato.");
        }
      },
      rifiutaModeMulti() {
        var that = this;
        var oList = this.getList();
        var oBinding = oList.getBinding("items");
        var items = oList.getSelectedItems();
        var list = [];
        if (items.length > 0) {
          for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var context = item.getBindingContext();
            var obj = context.getProperty(null, context);
            var object = {};
            object.Ebeln = obj.Ebeln;
            object.Frgco = obj.Frgco;
            object.FlagBudget = obj.FlagBudget;
            object.TestoApv = sap.ui
              .getCore()
              .byId("idFragment--richiesta_nota_reject")
              .getValue();
            object.StatoApv = "2";
            list.push(object);
          }
          var header = {
            Id: "1",
            OdaSet: list,
          };
          this.rifiutaSingle(header);
        } else {
          MessageToast.show("Nessun ordine di acquisto selezionato.");
        }
      },
      rifiutaSingle: function (object) {
        var that = this;
        if (
          sap.ui
            .getCore()
            .byId("idFragment--richiesta_nota_reject")
            .getValue() != ""
        ) {
          var oModelData1 = this.getOwnerComponent().getModel();
          oModelData1.create("/ContainerSet", object, {
            success: function (oRetrievedResult) {
              MessageToast.show("Salvata");
              sap.ui
                .getCore()
                .byId("idFragment--richiesta_nota_reject")
                .setValue("");
              that.closeDialogReject();
              that.getOwnerComponent().getModel().refresh();
              that.onCloseDetailPress();
            },
            error: function (oError) {
              console.log(oError);
              that.closeDialogReject();
            },
          });
        } else {
          MessageToast.show("Inserire il motivo rifiuto ");
        }
      },
      onCloseDetailPress: function () {
        this.getModel("appView").setProperty(
          "/actionButtonsInfo/midColumn/fullScreen",
          false
        );
        // No item should be selected on master after detail page is closed
        this.getOwnerComponent().oListSelector.clearMasterListSelection();
        this.getRouter().navTo("master");
      },

      returnLaunchpad: function () {
        window.location.replace(window.location.pathname);
      },
    });
  }
);
