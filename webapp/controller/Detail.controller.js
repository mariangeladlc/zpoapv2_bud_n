sap.ui.define(
  [
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library",
    "sap/ui/table/RowAction",
    "sap/ui/table/RowActionItem",
    "sap/ui/table/RowSettings",
    "sap/m/MessageToast",
  ],
  function (
    BaseController,
    JSONModel,
    formatter,
    mobileLibrary,
    RowAction,
    RowActionItem,
    RowSettings,
    MessageToast
  ) {
    "use strict";

    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;
    var oVizFrame = null;

    return BaseController.extend("com.stulz.zpoapv2budn.controller.Detail", {
      formatter: formatter,

      /* =========================================================== */
      /* lifecycle methods                                           */
      /* =========================================================== */

      onInit: function () {
        // Model used to manipulate control states. The chosen values make sure,
        // detail page is busy indication immediately so there is no break in
        // between the busy indication for loading the view's meta data
        var oViewModel = new JSONModel({
          busy: false,
          delay: 0,
          lineItemListTitle: this.getResourceBundle().getText(
            "detailLineItemTableHeading"
          ),
        });

        this.getRouter()
          .getRoute("object")
          .attachPatternMatched(this._onObjectMatched, this);

        this.setModel(oViewModel, "detailView");

        this.getOwnerComponent()
          .getModel()
          .metadataLoaded()
          .then(this._onMetadataLoaded.bind(this));
      },

      /* =========================================================== */
      /* event handlers                                              */
      /* =========================================================== */

      /**
       * Event handler when the share by E-Mail button has been clicked
       * @public
       */
      onSendEmailPress: function () {
        var oViewModel = this.getModel("detailView");

        URLHelper.triggerEmail(
          null,
          oViewModel.getProperty("/shareSendEmailSubject"),
          oViewModel.getProperty("/shareSendEmailMessage")
        );
      },

      /**
       * Updates the item count within the line item table's header
       * @param {object} oEvent an event containing the total number of items in the list
       * @private
       */
      onListUpdateFinished: function (oEvent) {
        var sTitle,
          iTotalItems = oEvent.getParameter("total"),
          oViewModel = this.getModel("detailView");

        // only update the counter if the length is final
        if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
          if (iTotalItems) {
            sTitle = this.getResourceBundle().getText(
              "detailLineItemTableHeadingCount",
              [iTotalItems]
            );
          } else {
            //Display 'Line Items' instead of 'Line items (0)'
            sTitle = this.getResourceBundle().getText(
              "detailLineItemTableHeading"
            );
          }
          oViewModel.setProperty("/lineItemListTitle", sTitle);
        }
      },
      onPress: function (oEvent) {
        // The source is the list item that got pressed
        this._showObject(oEvent.getSource());
      },
      _showObject: function (oItem) {
        console.log(oItem.getBindingContext().getProperty("Ebeln"));
        console.log(oItem.getBindingContext().getProperty("Ebelp"));
        this.getRouter().navTo("detailitem", {
          Ebeln: oItem.getBindingContext().getProperty("Ebeln"),
          Ebelp: oItem.getBindingContext().getProperty("Ebelp"),
        });
      },
      onBindingChange: function (oEvent) {
        this.getView()
          .byId("table")
          .setVisibleRowCount(oEvent.getSource().getLength());
        var oTable = this.byId("table");
        var fnPress = this.onPress.bind(this);
        var modes = [
          {
            key: "Navigation",
            text: "Navigation",
            handler: function () {
              var oTemplate = new RowAction({
                items: [
                  new RowActionItem({
                    type: "Navigation",
                    press: fnPress,
                    visible: "{Available}",
                  }),
                ],
              });
              return [1, oTemplate];
            },
          },
          {
            key: "NavigationDelete",
            text: "Navigation & Delete",
            handler: function () {
              var oTemplate = new RowAction({
                items: [
                  new RowActionItem({
                    type: "Navigation",
                    press: fnPress,
                    visible: "{Available}",
                  }),
                  new RowActionItem({
                    type: "Delete",
                    press: fnPress,
                  }),
                ],
              });
              return [2, oTemplate];
            },
          },
          {
            key: "NavigationCustom",
            text: "Navigation & Custom",
            handler: function () {
              var oTemplate = new RowAction({
                items: [
                  new RowActionItem({
                    type: "Navigation",
                    press: fnPress,
                    visible: "{Available}",
                  }),
                  new RowActionItem({
                    icon: "sap-icon://edit",
                    text: "Edit",
                    press: fnPress,
                  }),
                ],
              });
              return [2, oTemplate];
            },
          },
          {
            key: "Multi",
            text: "Multiple Actions",
            handler: function () {
              var oTemplate = new RowAction({
                items: [
                  new RowActionItem({
                    icon: "sap-icon://attachment",
                    text: "Attachment",
                    press: fnPress,
                  }),
                  new RowActionItem({
                    icon: "sap-icon://search",
                    text: "Search",
                    press: fnPress,
                  }),
                  new RowActionItem({
                    icon: "sap-icon://edit",
                    text: "Edit",
                    press: fnPress,
                  }),
                  new RowActionItem({
                    icon: "sap-icon://line-chart",
                    text: "Analyze",
                    press: fnPress,
                  }),
                ],
              });
              return [2, oTemplate];
            },
          },
          {
            key: "None",
            text: "No Actions",
            handler: function () {
              return [0, null];
            },
          },
        ];
        var iCount = 0;
        var oTemplate = oTable.getRowActionTemplate();
        if (oTemplate) {
          oTemplate.destroy();
          oTemplate = null;
        }
        for (var i = 0; i < modes.length; i++) {
          if ("Navigation" == modes[i].key) {
            var aRes = modes[i].handler();
            iCount = aRes[0];
            oTemplate = aRes[1];
            break;
          }
        }

        oTable.setRowActionTemplate(oTemplate);
        oTable.setRowActionCount(iCount);
      },
      /* =========================================================== */
      /* begin: internal methods                                     */
      /* =========================================================== */

      /**
       * Binds the view to the object path and expands the aggregated line items.
       * @function
       * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
       * @private
       */
      _onObjectMatched: function (oEvent) {
        var sObjectId = oEvent.getParameter("arguments").objectId;
        this.getModel("appView").setProperty(
          "/layout",
          "TwoColumnsMidExpanded"
        );
        this.getModel()
          .metadataLoaded()
          .then(
            function () {
              var sObjectPath = this.getModel().createKey("OdaSet", {
                Ebeln: sObjectId,
              });
              this._bindView("/" + sObjectPath);
            }.bind(this)
          );
      },

      /**
       * Binds the view to the object path. Makes sure that detail view displays
       * a busy indicator while data for the corresponding element binding is loaded.
       * @function
       * @param {string} sObjectPath path to the object to be bound to the view.
       * @private
       */
      _bindView: function (sObjectPath) {
        // Set busy indicator during view binding
        var oViewModel = this.getModel("detailView");

        // If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
        oViewModel.setProperty("/busy", false);

        this.getView().bindElement({
          path: sObjectPath,
          events: {
            change: this._onBindingChange.bind(this),
            dataRequested: function () {
              oViewModel.setProperty("/busy", true);
            },
            dataReceived: function () {
              oViewModel.setProperty("/busy", false);
            },
          },
        });
      },

      _onBindingChange: function () {
        var oView = this.getView(),
          oElementBinding = oView.getElementBinding();

        // No data for the binding
        if (!oElementBinding.getBoundContext()) {
          this.getRouter().getTargets().display("detailObjectNotFound");
          // if object could not be found, the selection in the master list
          // does not make sense anymore.
          this.getOwnerComponent().oListSelector.clearMasterListSelection();
          return;
        }

        var sPath = oElementBinding.getPath(),
          oResourceBundle = this.getResourceBundle(),
          oObject = oView.getModel().getObject(sPath),
          sObjectId = oObject.Ebeln,
          sObjectName = oObject.Ebeln,
          oViewModel = this.getModel("detailView");
        this.setDetail(oObject);

        this.getOwnerComponent().oListSelector.selectAListItem(sPath);

        oViewModel.setProperty(
          "/shareSendEmailSubject",
          oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId])
        );
        oViewModel.setProperty(
          "/shareSendEmailMessage",
          oResourceBundle.getText("shareSendEmailObjectMessage", [
            sObjectName,
            sObjectId,
            location.href,
          ])
        );

        var oModelData1 = this.getOwnerComponent().getModel();
        var that = this;
        oModelData1.read("/OdaSet('" + sObjectId + "')/DocumentoSet", {
          success: function (oObjectRes) {
            console.log(oObjectRes.results);
            var oViewModel = that.getModel("detailView");
            oViewModel.setProperty("/numFile", oObjectRes.results.length);
          },
          error: function (oError) {
            console.log(oError);
          },
        });
      },

      _onMetadataLoaded: function () {
        // Store original busy indicator delay for the detail view
        var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
          oViewModel = this.getModel("detailView");
        // Make sure busy indicator is displayed immediately when
        // detail view is displayed for the first time
        oViewModel.setProperty("/delay", 0);
        oViewModel.setProperty("/lineItemTableDelay", 0);

        // Binding the view will set it to not busy - so the view is always busy if it is not bound
        oViewModel.setProperty("/busy", true);
        // Restore original busy indicator delay for the detail view
        oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
      },

      /**
       * Set the full screen mode to false and navigate to master page
       */
      onCloseDetailPress: function () {
        this.getModel("appView").setProperty(
          "/actionButtonsInfo/midColumn/fullScreen",
          false
        );
        // No item should be selected on master after detail page is closed
        this.getOwnerComponent().oListSelector.clearMasterListSelection();
        this.getRouter().navTo("master");
        this.setDetail(null);
      },

      /**
       * Toggle between full and non full screen mode.
       */
      toggleFullScreen: function () {
        var bFullScreen = this.getModel("appView").getProperty(
          "/actionButtonsInfo/midColumn/fullScreen"
        );
        this.getModel("appView").setProperty(
          "/actionButtonsInfo/midColumn/fullScreen",
          !bFullScreen
        );
        if (!bFullScreen) {
          // store current layout and go full screen
          this.getModel("appView").setProperty(
            "/previousLayout",
            this.getModel("appView").getProperty("/layout")
          );
          this.getModel("appView").setProperty(
            "/layout",
            "MidColumnFullScreen"
          );
        } else {
          // reset to previous layout
          this.getModel("appView").setProperty(
            "/layout",
            this.getModel("appView").getProperty("/previousLayout")
          );
        }
      },
      formatterIcon: function (value, flg) {
        console.log(flg);
        if (flg == "X") {
          return "sap-icon://status-error";
        } else {
          switch (value) {
            case "1":
              return "sap-icon://accept";
              break;
            case "2":
              return "sap-icon://cancel";
              break;
            case "3":
              return "sap-icon://warning";
              break;

            default:
              return "sap-icon://email";
              break;
          }
        }
      },

      formatterIconBudget: function (value) {
        console.log(value);
        if (value == "X") {
          return "sap-icon://alert";
        }
        return "";
      },

      openDialogNote: function () {
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = bindingContext.getModel().getProperty(path);
        this._getDialogNote().open();
      },
      _getDialogNote: function () {
        if (!this._oDialogNote) {
          this._oDialogNote = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zpoapv2budn.view.DialogNote",
            this
          );
          this.getView().addDependent(this._oDialogNote);
        }
        return this._oDialogNote;
      },
      /*
      _getDialogApprove: function () {
        if (!this._oDialogApprove) {
          this._oDialogApprove = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zpoapv2budn.view.DialogApprove",
            this
          );
          this.getView().addDependent(this._oDialogApprove);
        }
        return this._oDialogApprove;
      },
      _getDialogReject: function () {
        if (!this._oDialogReject) {
          this._oDialogReject = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zpoapv2budn.view.DialogReject",
            this
          );
          this.getView().addDependent(this._oDialogReject);
        }
        return this._oDialogReject;
      },

openDialogApprove: function () {
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = bindingContext.getModel().getProperty(path);
        this._getDialogApprove().open();
      },
      openDialogReject: function () {
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = bindingContext.getModel().getProperty(path);
        this._getDialogReject().open();
      },
      closeDialogApprove: function () {
        this._oDialogApprove.close();
      },
      closeDialogReject: function () {
        this._oDialogReject.close();
      },
      approva: function () {
        var that = this;

        var oView = this.getView(),
          oElementBinding = oView.getElementBinding();

        var sPath = oElementBinding.getPath(),
          oResourceBundle = this.getResourceBundle(),
          oObject = oView.getModel().getObject(sPath),
          Ebeln = oObject.Ebeln;

        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = {};

        object.Ebeln = Ebeln;
        object.TestoApv = sap.ui
          .getCore()
          .byId("idFragment--richiesta_nota")
          .getValue();
        object.StatoApv = "1";
        console.log(object);
        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.update(path, object, {
          success: function (oRetrievedResult) {
            MessageToast.show("Salvata");
            sap.ui.getCore().byId("idFragment--richiesta_nota").setValue("");

            that.closeDialogApprove();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
            that.onCloseDetailPress();
          },
          error: function (oError) {
            console.log(oError);
            that.closeDialogApprove();
          },
        });
      },

      rifiuta: function () {
        var that = this;

        var oView = this.getView(),
          oElementBinding = oView.getElementBinding();

        var sPath = oElementBinding.getPath(),
          oResourceBundle = this.getResourceBundle(),
          oObject = oView.getModel().getObject(sPath),
          Ebeln = oObject.Ebeln;

        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = {};

        object.Ebeln = Ebeln;
        object.TestoApv = sap.ui
          .getCore()
          .byId("idFragment--richiesta_nota_reject")
          .getValue();
        object.StatoApv = "2";
        console.log(object);

        if (
          sap.ui
            .getCore()
            .byId("idFragment--richiesta_nota_reject")
            .getValue() != ""
        ) {
          var oModelData1 = this.getOwnerComponent().getModel();
          oModelData1.update(path, object, {
            success: function (oRetrievedResult) {
              MessageToast.show("Salvata");
              sap.ui
                .getCore()
                .byId("idFragment--richiesta_nota_reject")
                .setValue("");

              that.closeDialogReject();
              that.getOwnerComponent().getModel().refresh();
              that.onCloseDetailPress();

              //refresh di tutti i componenti
            },
            error: function (oError) {
              console.log(oError);
              that.closeDialogReject();
            },
          });
        } else {
          MessageToast.show("Inserire il motivo rifiuto ");
        }
      },
      */
      closeDialogNota: function () {
        this._oDialogNote.close();
      },
      saveNota: function () {
        var that = this;
        var oView = this.getView(),
          oElementBinding = oView.getElementBinding(),
          sPath = oElementBinding.getPath(),
          oObject = oView.getModel().getObject(sPath),
          sObjectId = oObject.Ebeln;

        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = {
          Ebeln: sObjectId,
          Nota: sap.ui.getCore().byId("idFragment--nota").getValue(),
        };
        console.log(object);
        var oModelData1 = this.getOwnerComponent().getModel();
        oModelData1.create("/CronologiaSet", object, {
          success: function (oRetrievedResult) {
            MessageToast.show("Salvata");
            sap.ui.getCore().byId("idFragment--nota").setValue("");

            that.closeDialogNota();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
          },
          error: function (oError) {
            console.log(oError);
            that.closeDialogNota();
          },
        });
      },

      formatterDate: function (date, time) {
        // SAPUI5 formatters
        var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
          pattern: "dd/MM/yyyy",
        });
        var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
          pattern: "KK:mm:ss a",
        });
        // timezoneOffset is in hours convert to milliseconds
        var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
        // format date and time to strings offsetting to GMT
        var dateStr = dateFormat.format(new Date(date.getTime() + TZOffsetMs)); //05-12-2012
        var timeStr = timeFormat.format(new Date(time.ms + TZOffsetMs)); //11:00 AM
        //parse back the strings into date object back to Time Zone
        var parsedDate = new Date(
          dateFormat.parse(dateStr).getTime() - TZOffsetMs
        ); //1354665600000
        var parsedTime = new Date(
          timeFormat.parse(timeStr).getTime() - TZOffsetMs
        ); //39600000

        console.log(dateStr + timeStr);
        return dateStr + "  " + timeStr;
      },

      onDownloadItemSmart: function (event) {
        console.log("onDownloadItemSmart");
        var that = this;

        console.log(event.getSource().getBindingContext().getObject());
        var Ebeln = event.getSource().getBindingContext().getObject().Ebeln;
        var Ext = event.getSource().getBindingContext().getObject().Ext;
        var ArcDocId = event.getSource().getBindingContext().getObject()
          .ArcDocId;
        console.log(ArcDocId);

        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.read(
          "/DownloadSet(Ebeln='" + Ebeln + "',ArcDocId='" + ArcDocId + "')",
          {
            success: function (oRetrievedResult) {
              console.log(oRetrievedResult.Datadoc);

              var contentType = "";

              switch (Ext) {
                case "DOCX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                  break;
                case "DOC":
                  contentType = "application/msword";
                  break;
                case "XLSX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                  break;
                case "XLS":
                  contentType = "application/vnd.ms-excel";
                  break;
                case "PPT":
                  contentType = "application/vnd.ms-powerpoint";
                  break;
                case "PPTX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                  break;
                case "PNG":
                  contentType = "image/png";
                  break;
                case "JPEG":
                  contentType = "image/png";
                  break;
                case "JPG":
                  contentType = "image/png";
                  break;
                case "SVG":
                  contentType = "image/svg+xml";
                  break;
                case "PDF":
                  contentType = "application/pdf";
                  break;
                case "MSG":
                  contentType = "octet/stream";
                  break;

                default:
                  contentType = "application/*";
                  break;
              }

              var a =
                "data:" +
                contentType +
                ";base64," +
                oRetrievedResult.Datadoc +
                "";
              var o = document.createElement("a");
              var i = oRetrievedResult.ArcDocId;
              o.href = a;
              if (Ext != "MSG") {
                o.download = i;
              } else {
                o.download = i + ".msg";
              }
              o.click();
            },
            error: function (oError) {
              console.log(oError);
            },
          }
        );
      },
      onDownloadItemSmart2: function (event) {
        console.log("onDownloadItemSmart");
        var that = this;

        console.log(event.getSource().getBindingContext().getObject());
        var Ebeln = event.getSource().getBindingContext().getObject().Ebeln;
        var Ext = event.getSource().getBindingContext().getObject().Ext;
        var ArcDocId = event.getSource().getBindingContext().getObject()
          .ArcDocId;
        console.log(ArcDocId);

        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.read(
          "/DownloadSet(Ebeln='" + Ebeln + "',ArcDocId='" + ArcDocId + "')",
          {
            success: function (oRetrievedResult) {
              console.log(oRetrievedResult.Datadoc);

              var contentType = "";

              switch (Ext) {
                case "DOCX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                  break;
                case "DOC":
                  contentType = "application/msword";
                  break;
                case "XLSX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                  break;
                case "XLS":
                  contentType = "application/vnd.ms-excel";
                  break;
                case "PPT":
                  contentType = "application/vnd.ms-powerpoint";
                  break;
                case "PPTX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                  break;
                case "PNG":
                  contentType = "image/png";
                  break;
                case "JPEG":
                  contentType = "image/png";
                  break;
                case "JPG":
                  contentType = "image/png";
                  break;
                case "SVG":
                  contentType = "image/svg+xml";
                  break;
                case "PDF":
                  contentType = "application/pdf";
                  break;
                case "MSG":
                  contentType = "octet/stream";
                  break;

                default:
                  contentType = "application/*";
                  break;
              }
              if (Ext != "MSG") {
                var b64Data = oRetrievedResult.Datadoc;
                var blob = that.b64toBlob(b64Data, contentType);
                var blobUrl = URL.createObjectURL(blob);
                window.open(blobUrl);
              } else {
                var a =
                  "data:" +
                  contentType +
                  ";base64," +
                  oRetrievedResult.Datadoc +
                  "";
                var o = document.createElement("a");
                var i = oRetrievedResult.ArcDocId;
                o.href = a;
                o.download = i + ".msg";
                o.click();
              }
            },
            error: function (oError) {
              console.log(oError);
            },
          }
        );
      },
      b64toBlob: function (b64Data, contentType) {
        var sliceSize = 512;
        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (
          let offset = 0;
          offset < byteCharacters.length;
          offset += sliceSize
        ) {
          const slice = byteCharacters.slice(offset, offset + sliceSize);

          const byteNumbers = new Array(slice.length);
          for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }

          const byteArray = new Uint8Array(byteNumbers);
          byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
      },
    });
  }
);
