sap.ui.define([
	"./BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("com.stulz.zpoapv2budn.controller.DetailObjectNotFound", {});
});
