sap.ui.define(
  [
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library",
    "sap/ui/table/RowAction",
    "sap/ui/table/RowActionItem",
    "sap/ui/table/RowSettings",
    "sap/m/MessageToast",
    "sap/ui/core/routing/History",
  ],
  function (
    BaseController,
    JSONModel,
    formatter,
    mobileLibrary,
    RowAction,
    RowActionItem,
    RowSettings,
    MessageToast,
    History
  ) {
    "use strict";

    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;
    var oVizFrame = null;

    return BaseController.extend(
      "com.stulz.zpoapv2budn.controller.DetailItem",
      {
        onInit: function () {
          var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          var oViewModel = new JSONModel({
            busy: false,
            delay: 0,
            lineItemListTitle: this.getResourceBundle().getText(
              "detailLineItemTableHeading"
            ),
          });

          this.setModel(oViewModel, "detailItemView");

          oRouter
            .getRoute("detailitem")
            .attachPatternMatched(this._onObjectMatched, this);
          oVizFrame = this.getView().byId("idpiechart");

          var new_2 = {
            Sales: [],
          };
          var oModel2 = new sap.ui.model.json.JSONModel(new_2);
          this.byId("idpiechart").setModel(oModel2);

          var oDataset = new sap.viz.ui5.data.FlattenedDataset({
            dimensions: [
              {
                name: "BudgetUtil",
                value: "{BudgetUtil}",
              },
            ],

            measures: [
              {
                name: "BudgetRes",
                value: "{BudgetRes}",
              },
            ],

            data: {
              path: "/Sales",
            },
          });
          oVizFrame.setDataset(oDataset);

          //      4.Set Viz properties
          oVizFrame.setVizProperties({
            title: {
              text: "BUDGET",
            },
            plotArea: {
              colorPalette: d3.scale.category20().range(),
              drawingEffect: "glossy",
            },
          });

          var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({
              uid: "size",
              type: "Measure",
              values: ["BudgetRes"],
            }),
            feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({
              uid: "color",
              type: "Dimension",
              values: ["BudgetUtil"],
            });
          oVizFrame.addFeed(feedSize);
          oVizFrame.addFeed(feedColor);

          var donutProp = {
            plotArea: {
              dataLabel: {
                visible: true,
              },
            },
            legend: {
              title: {
                visible: false,
              },
            },
            legendGroup: {
              layout: {
                position: "bottom",
              },
            },
            title: { text: "", visible: false },
            valueAxis: {
              title: {
                visible: false,
              },
              label: {
                formatString:
                  sap.viz.ui5.format.ChartFormatter.DefaultPattern.LONGFLOAT,
              },
            },
            categoryAxis: {
              title: {
                visible: false,
              },
            },
          };

          oVizFrame.setVizProperties(donutProp);
        },

        _onObjectMatched: function (oEvent) {
          //Bind the Context to Detail View
          //            this.getView().bindElement({
          //               path: "/" + oEvent.getParameter("arguments").Ebeln,
          //              model: "view"
          //         });

          var Ebeln = oEvent.getParameter("arguments").Ebeln;
          var Ebelp = oEvent.getParameter("arguments").Ebelp;
          this.getModel()
            .metadataLoaded()
            .then(
              function () {
                var sObjectPath = this.getModel().createKey("OdaItemSet", {
                  Ebeln: Ebeln,
                  Ebelp: Ebelp,
                });
                this._bindView("/" + sObjectPath);
              }.bind(this)
            );
        },

        _bindView: function (sObjectPath) {
          console.log(sObjectPath);
          // Set busy indicator during view binding
          var oViewModel = this.getModel("detailItemView");
          console.log(oViewModel);

          // If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
          oViewModel.setProperty("/busy", false);

          this.getView().bindElement({
            path: sObjectPath,
            events: {
              change: this._onBindingChange.bind(this),
              dataRequested: function () {
                oViewModel.setProperty("/busy", true);
              },
              dataReceived: function () {
                oViewModel.setProperty("/busy", false);
              },
            },
          });
        },

        onNavBack: function () {
          history.go(-1);
        },

        _onBindingChange: function () {
          var oView = this.getView(),
            oElementBinding = oView.getElementBinding();
          console.log(oElementBinding);
          // No data for the binding
          if (!oElementBinding.getBoundContext()) {
            this.getRouter().getTargets().display("detailObjectNotFound");
            // if object could not be found, the selection in the master list
            // does not make sense anymore.
            this.getOwnerComponent().oListSelector.clearMasterListSelection();
            return;
          }

          var sPath = oElementBinding.getPath(),
            oResourceBundle = this.getResourceBundle(),
            oObject = oView.getModel().getObject(sPath),
            sObjectId = oObject.Ebeln,
            sObjectName = oObject.Ebeln,
            oViewModel = this.getModel("detailItemView");
          console.log(oObject);
          this.getOwnerComponent().oListSelector.selectAListItem(sPath);

          oViewModel.setProperty(
            "/shareSendEmailSubject",
            oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId])
          );
          oViewModel.setProperty(
            "/shareSendEmailMessage",
            oResourceBundle.getText("shareSendEmailObjectMessage", [
              sObjectName,
              sObjectId,
              location.href,
            ])
          );

          var oModelData1 = this.getOwnerComponent().getModel();

          console.log(
            "QUESTO è IL PATH : " +
              "/OdaItemSet(Ebeln=" +
              oObject.Ebeln +
              ",Ebelp=" +
              oObject.Ebelp +
              ")/Budget"
          );
          oModelData1.read(
            "/OdaItemSet(Ebeln='" +
              oObject.Ebeln +
              "',Ebelp='" +
              oObject.Ebelp +
              "')/Budget",
            {
              success: function (oObjectBudget) {
                console.log("risultato chiamata budget");
                console.log(oObjectBudget);

                var oModel = new sap.ui.model.json.JSONModel();
                var data = {
                  Sales: [
                    {
                      BudgetUtil: "Budget Utilizzato",
                      BudgetRes: oObjectBudget.BudgetUtil,
                    },
                    {
                      BudgetUtil: "Budget Residuo",
                      BudgetRes: oObjectBudget.BudgetRes,
                    },
                  ],
                };
                oModel.setData(data);
                oVizFrame.setModel(oModel);

                var oDataset = new sap.viz.ui5.data.FlattenedDataset({
                  dimensions: [
                    {
                      name: "BudgetUtil",
                      value: "{BudgetUtil}",
                    },
                  ],

                  measures: [
                    {
                      name: "BudgetRes",
                      value: "{BudgetRes}",
                    },
                  ],

                  data: {
                    path: "/Sales",
                  },
                });
                oVizFrame.setDataset(oDataset);
              },
              error: function (oError) {
                console.log(oError);
              },
            }
          );
        },

        /*
		_onObjectMatched: function(oEvent) {

        	var Ebeln =  oEvent.getParameter("arguments").Ebeln;
        	var Ebelp =  oEvent.getParameter("arguments").Ebelp;
            console.log(Ebeln,Ebelp);

			this.getModel().metadataLoaded().then( function() {
				var sObjectPath = this.getModel().createKey("OdaSet", {
					Ebeln :  sObjectId
				});
				this._bindView("/" + sObjectPath);

            this.getView().bindElement({
				path: "/" + oEvent.getParameter("arguments").invoicePath,
				model: "invoice"
			});

        var that = this;
        var oModelData1 = new sap.ui.model.odata.v2.ODataModel(
          "/sap/opu/odata/sap/ZPOAPV_PROJECT_srv"
        );

        oModelData1.read("/OdaItemSet(Ebeln='"+Ebeln+"',Ebelp='"+Ebelp+"')"),
          {
            success: function (oObjectRes) {
              console.log(oObjectRes);
                that.setModel("detailView",oObjectRes.results);

            },
            error: function (oError) {
              console.log(oError);
            },
          }



        },
            
*/
      }
    );
  }
);
