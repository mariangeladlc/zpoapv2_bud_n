/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"com/stulz/zpoapv2budn/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});